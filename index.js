const express = require('express');
const mongoose = require('mongoose');
const movies = require('./routes/movies');
const morgan = require('morgan');
const app = express();

mongoose.connect('mongodb+srv://saiganeshdarni:Rgukt%401234@cluster0.y4v1q6e.mongodb.net/movie')
.then(() => console.log('Connected to db'))
.catch(err => console.log('could not connect to mongoDB...',err));

app.set('view engine','ejs');
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(express.static('public'));

app.use(morgan('dev'));

app.use('/movies',movies);

app.use('/',(req,res) => {
    res.redirect('/movies');
}
);


app.listen(3000, () => {
    console.log('Server is running on port 3000');
}
);


