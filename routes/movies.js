const express = require('express');
const router = express.Router();
const Movie = require('../models/Movie');


router.get('/', (req, res) => {
    Movie.find().sort({ createdAt: -1})
        .then((result) => {
            // res.render('/movies/index',{title: 'All Movies',movies: result})
            return res.json(result);
        })
        .catch((err) => {
            console.log(err);
        })
});

// router.get('/:id', (req, res) => {
//     const id = req.params.id;
//     Movie.findById(id)
//         .then((result) => {
//             res.render('movies/details', { movie: result,title: 'Movie Details'});
//         })
//         .catch((err) => {
//             console.log(err);
//         })
// });

router.get('/create', (req, res) => {
    // res.render('movies/create',{title:'Create'});
    return res.json({title: 'Create'});
});

router.post('/', (req, res) => {
    const movie = new Movie(req.body)
    movie.save()
        .then((result) => {
            res.redirect('/movies');
        })
        .catch((err) => {
            console.log(err);
        })
    return res.json(req.body);
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    Movie.findByIdAndDelete(id)
        .then(result => {
            res.json({ redirect: '/movies'});
        })
        .catch(err =>{
            console.log(err);
        })
    return res.json({id});
});

router.put('/:id',(req,res) => {
    const id = req.params.id;
    Movie.findByIdAndUpdate(id, req.body)
        .then(result => {
            res.json({ redirect: '/movies'});
        })
        .catch(err => {
            console.log(err);
        })
    return res.json({id});
});


    
router.put('/:id/favourite', (req, res) => {
    const id = req.params.id;
    Movie.findByIdAndUpdate(id, {isFavorite: true})
        .then(result => {
            res.json({ redirect: '/movies'});
        })
        .catch(err =>{
            console.log(err);
        })
    return res.json({id});
}); 

router.put('/:id/unfavourite', (req, res) => {
    const id = req.params.id;
    Movie.findByIdAndUpdate(id, {isFavorite: false})
        .then(result => {
            res.json({ redirect: '/movies'});
        })
        .catch(err =>{
            console.log(err);
        })
    return res.json({id});
});

//All favourite movies
router.get('/favourite', (req, res) => {
    Movie.find({isFavorite: true})
        .then((result) => {
            return res.json(result);
        })
        .catch((err) => {
            console.log(err);
        })
    // return res.json({title: 'Favourite Movies'});   
});



module.exports = router;                    

    